## ThreeJS transparent-video-in-sphere demo
Using a custom shader, this illustrates how to successfully play 2D videos with transparent backgrounds in a 3D sphere.
### Preview:
![](preview.jpg)